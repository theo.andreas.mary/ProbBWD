function C = new_matmul(A,B,prec,type)
  [m,n] = size(A);
  [~,p] = size(B);
  C = zeros(m,p);
  t = 11;

  if strcmp(type,'classical')
    if strcmp(prec,'single')
      for kk = 1:n
        C = C + A(:,kk)*B(kk,:);
      end
    elseif strcmp(prec,'half')
      for kk = 1:n
        C = chop(C + chop(A(:,kk)*B(kk,:),t),t);
      end
    end
  elseif strcmp(type,'new')
    x = mean(A,2); y = ones(1,n);
    Anew = A - x*y;
    if strcmp(prec,'single')
      Anew = single(Anew);
      for kk = 1:n
        C = C + Anew(:,kk)*B(kk,:);
      end
      C = double(C);
    elseif strcmp(prec,'half')
      for kk = 1:n
        C = chop(C + chop(Anew(:,kk)*B(kk,:),t),t);
      end
    end
    C = C + x*(y*B);
  end
end

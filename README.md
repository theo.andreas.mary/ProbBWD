# ProbBWD

MATLAB codes to perform the numerical experiments reported in the article
"Sharper Probabilistic Backward Error Analysis for Basic Linear Algebra
Kernels" by N. J. Higham and T. Mary.

## Included MATLAB files
* **test_summation.m**: generates Figures 1.1 and 2.1.
* **test_inner.m**: generates Figure 3.1(a).
* **test_matvec.m**: generates Figure 3.1(b).
* **new_matmul.m**: implements Algorithm 4.2.
* **test_new_matmul_single.m** and **test_new_matmul_half.m**: generate Figures 4.1(a) and 4.1(b).
* **test_FABsum_single.m** and **test_FABsum_half.m**: generate Figures 4.2(a) and 4.2(b).

## Requirements
* The codes have been developed and tested with MATLAB 2018b.
* The scripts **test_new_matmul_half.m** and **test_FABsum_half.m** require the
  function **chop.m** from the [Matrix Computations
Toolbox](http://www.ma.man.ac.uk/~higham/mctoolbox/) to simulate the use of
half precision.

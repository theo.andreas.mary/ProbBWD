clear all;
close all;

nlist  = round(logspace(1,5,20));
m = 32;
p = 32;
t = 11;

rng(1)
for j=1:length(nlist)
  n = nlist(j);
  fprintf('%g of %g nvals (n = %g)\n',j,length(nlist),n)
  A = rand(m,n); B = rand(n,p); 
  A = chop(A,t); B = chop(B,t);

  C0 = A*B;
  C1 = new_matmul(A,B,'half','classical');
  C2 = new_matmul(A,B,'half','new');

  err1(j) = max(max(abs(C1./C0-1)));
  err2(j) = max(max(abs(C2./C0-1)));
end
u = 2^-t;
probound = sqrt(nlist)*u;

fs=14; ms=10; lw=1;
loglog(nlist, probound, '--k', 'LineWidth',lw);
hold on;
loglog(nlist, err1, '-o','markersize',ms,'linewidth',lw);
loglog(nlist, err2, '-*','markersize',ms,'linewidth',lw);
hleg=legend('$$\sqrt{n}u$$', 'Classical', 'New', 'Location', 'NorthWest');
set(hleg,'fontsize',fs,'interpreter','latex');
xlabel('$$n$$','interpreter','latex','fontsize',fs);
set(gca,'fontsize',fs);


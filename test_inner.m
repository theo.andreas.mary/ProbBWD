clear all;
close all;

u      = 2^-24;
nlist  = round(logspace(2,5));

berr = zeros(length(nlist),4);
for data = [1 2 3 4]
  rng(1)
  ntest = 100;
  for j=1:length(nlist)
    n = nlist(j);
    for itest=1:ntest
      fprintf('%d/4 data, %d/%d size (n=%d), %d/%d test\n',data,j,length(nlist),n,itest,ntest);
      switch(data)
      case 1
        x = rand(n,1); 
        y = rand(n,1); 
      case 2
        x = (rand(n,1)-0.5)*2; 
        y = (rand(n,1)-0.5)*2; 
      case 3
        x = rand(n,1); 
        y = (rand(n,1)-0.5)*2; 
      case 4
        x = (rand(n,1)-0.5)*2; 
        y = rand(n,1); 
      end
      x = single(x);
      y = single(y);

      se = double(x)'*double(y);
      s = 0;
      for i=1:n
        s = s + x(i)*y(i);
      end
      s = double(s);
      absxy = double(abs(x))'*double(abs(y));
      berr(j,data) = max(berr(j,data), abs(se-s)/absxy);
    end
  end
end
probound = sqrt(nlist)*u;
  
figure(1);
fs=14; ms=7; lw=1;
loglog(nlist,berr(:,1),'-o','LineWidth',lw,'Markersize',ms);
hold on;
loglog(nlist,berr(:,2),'-v','LineWidth',lw,'Markersize',ms);
loglog(nlist,berr(:,3),'-s','LineWidth',lw,'Markersize',ms);
loglog(nlist,berr(:,4),'-*','LineWidth',lw,'Markersize',ms);
loglog(nlist,probound,'--k','LineWidth',lw);
hleg = legend('$$x,y\in[0,1]$$', '$$x,y\in[-1,1]$$', '$$x\in[0,1], y\in[-1,1]$$', '$$x\in[-1,1], y\in[0,1]$$');
xlabel('$$n$$','interpreter','latex');
xlim([-inf inf]);
ylim([-inf inf]);
yticks(10.^(-8:0));
set(gca,'fontsize',fs);
set(hleg,'fontsize',fs,'interpreter','latex','location','northwest');


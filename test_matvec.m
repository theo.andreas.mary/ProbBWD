clear all;
close all;

u      = 2^-24;
nlist  = round(logspace(2,5));

berr = zeros(length(nlist),4);
for data = [1 2 3 4]
  rng(1)
  ntest = 1; m = 100;
  for j=1:length(nlist)
    n = nlist(j);
    for itest=1:ntest
      fprintf('%d/4 data, %d/%d size (n=%d), %d/%d test\n',data,j,length(nlist),n,itest,ntest);
      switch(data)
      case 1
        A = rand(m,n); 
        x = rand(n,1); 
      case 2
        A = (rand(m,n)-0.5)*2; 
        x = (rand(n,1)-0.5)*2; 
      case 3
        A = rand(m,n); 
        x = (rand(n,1)-0.5)*2; 
      case 4
        A = (rand(m,n)-0.5)*2; 
        x = rand(n,1); 
      end
      A = single(A);
      x = single(x);

      ye = double(A)*double(x);
      y = 0;
      for k=1:n
        y = y + A(:,k)*x(k);
      end
      y = double(y);
      absAx = double(abs(A))*double(abs(x));
      berr(j,data) = max(berr(j,data), max(abs(ye-y)./absAx));
    end
  end
end
probound = sqrt(nlist)*u;
  
fs=14; ms=7; lw=1;
figure(1);
loglog(nlist,berr(:,1),'-o','LineWidth',lw,'Markersize',ms);
hold on;
loglog(nlist,berr(:,2),'-v','LineWidth',lw,'Markersize',ms);
loglog(nlist,berr(:,3),'-s','LineWidth',lw,'Markersize',ms);
loglog(nlist,berr(:,4),'-*','LineWidth',lw,'Markersize',ms);
loglog(nlist,probound,'--k','LineWidth',lw);
hleg = legend('$$A,x\in[0,1]$$', '$$A,x\in[-1,1]$$', '$$A\in[0,1], x\in[-1,1]$$', '$$A\in[-1,1], x\in[0,1]$$');
xlabel('$$n$$','interpreter','latex');
xlim([-inf inf]);
ylim([-inf inf]);
yticks(10.^(-8:0));
set(gca,'fontsize',fs);
set(hleg,'fontsize',fs,'interpreter','latex','location','northwest');


clear all;
close all;

u      = 2^-24;
nlist  = round(logspace(2,6));

berr = zeros(length(nlist),3);
ferr = zeros(length(nlist),3);
for data = [1 2]
  rng(1)
  ntest = 10;
  for j=1:length(nlist)
    n = nlist(j);
    for itest=1:ntest
      fprintf('%d/2 data, %d/%d size (n=%d), %d/%d test\n',data,j,length(nlist),n,itest,ntest);
      switch(data)
      case 1
        a = rand(n,1); 
      case 2
        a = (rand(n,1)-0.5)*2; 
      end
      a = single(a);

      se = sum(double(a));
      s = 0;
      for i=1:n
        s = s + a(i);
      end
      s = double(s);
      berr(j,data) = max(berr(j,data), abs(se-s)/sum(double(abs(a))));
      ferr(j,data) = max(ferr(j,data), abs(se-s)/abs(se));
    end
  end
end
detbound = nlist*u;
probound = sqrt(nlist)*u;
  
fs=14; ms=7; lw=1;
figure(1);
loglog(nlist,detbound,'-k','LineWidth',lw);
hold on;
loglog(nlist,probound,'--k','LineWidth',lw);
loglog(nlist,berr(:,1),'-o','LineWidth',lw,'Markersize',ms);
loglog(nlist,berr(:,2),'-*','LineWidth',lw,'Markersize',ms);
hleg = legend('$$nu$$','$$\sqrt{n}u$$','bwderr ([0,1])', 'bwderr ([-1,1])');
xlabel('$$n$$','interpreter','latex');
xlim([nlist(1) nlist(end)]);
ylim([6e-9 1]);
yticks(10.^(-8:2:0));
set(gca,'fontsize',fs);
set(hleg,'fontsize',fs,'interpreter','latex','location','northwest');

figure(2);
loglog(nlist,detbound,'-k','LineWidth',lw);
hold on;
loglog(nlist,probound,'--k','LineWidth',lw);
loglog(nlist,ferr(:,1),'-o','LineWidth',lw,'Markersize',ms);
loglog(nlist,ferr(:,2),'-*','LineWidth',lw,'Markersize',ms);
hleg = legend('$$nu$$','$$\sqrt{n}u$$','fwderr ([0,1])', 'fwderr ([-1,1])');
xlabel('$$n$$','interpreter','latex');
xlim([nlist(1) nlist(end)]);
ylim([6e-9 1]);
yticks(10.^(-8:2:0));
set(gca,'fontsize',fs);
set(hleg,'fontsize',fs,'interpreter','latex','location','northwest');


clear all;
close all;

m = 16;
p = 16;
nlist  = round(logspace(2,7,20));

rng(1)
for j=1:length(nlist)
  n = nlist(j);
  fprintf('%g of %g nvals (n = %g)\n',j,length(nlist),n)
  A = rand(m,n); B = rand(n,p); 
  A = single(A); B = single(B);
  x = mean(A,2); y = ones(1,n);
  Anew = single(double(A) - x*y);

  Cex = double(A)*double(B);

  C1 = double(matmul_compensated(A,B));
  C2 = double(matmul_FABsum_compensated(A,B,32));
  C3 = double(matmul_FABsum_compensated(A,B,128));
  C4 = double(matmul_single(A,B,'new'));

  err1(j) = max(max(abs(C1-Cex)./Cex));
  err2(j) = max(max(abs(C2-Cex)./Cex));
  err3(j) = max(max(abs(C3-Cex)./Cex));
  err4(j) = max(max(abs(C4-Cex)./Cex));
end

fs=14; ms=7; lw=1;
loglog(nlist, err1, '-o','markersize',ms,'linewidth',lw);
hold on;
loglog(nlist, err2, '-v','markersize',ms,'linewidth',lw);
loglog(nlist, err3, '-s','markersize',ms,'linewidth',lw);
loglog(nlist, err4, '-*','markersize',ms,'linewidth',lw);
hleg=legend('Compensated', 'FABsum ($$b=16$$)', 'FABsum ($$b=128$$)', 'New', 'Location', 'NorthEast');
set(hleg,'fontsize',fs,'interpreter','latex');
xlabel('$$n$$','interpreter','latex','fontsize',fs);
xlim([1e2 1e7]);
ylim([1e-8 1e-6]);
xticks([1e2 1e3 1e4 1e5 1e6 1e7]);
set(gca,'fontsize',fs);

function C = matmul_compensated(A,B)
  [m,n] = size(A);
  [~,p] = size(B);
  C = zeros(m,p);
  E = zeros(m,p);
  for k = 1:n
    T = C;
    Y = E + A(:,k)*B(k,:);
    C = T+Y;
    E = (T-C) + Y;
  end
end

function C = matmul_FABsum_compensated(A,B,nb)
  [m,n] = size(A);
  [~,p] = size(B);
  C = zeros(m,p);
  E = zeros(m,p);
  npart = ceil(n/nb);
  for k = 1:npart
    ibeg = 1+(k-1)*nb;
    iend = min(k*nb,n);
    T = C;
    Y = E + matmul_single(A(:,ibeg:iend),B(ibeg:iend,:),'std');
    C = T+Y;
    E = (T-C) + Y;
  end
end


clear all;
close all;

t = 11;
m = 16;
p = 16;
nlist  = round(logspace(2,5,20));

rng(1)
for j=1:length(nlist)
  n = nlist(j);
  fprintf('%g of %g nvals (n = %g)\n',j,length(nlist),n)
  A = rand(m,n); B = rand(n,p); 
  A = chop(A,t); B = chop(B,t);

  C0 = A*B;
  C1 = matmul_compensated(A,B,t);
  C2 = matmul_FABsum_compensated(A,B,t,16);
  C3 = matmul_FABsum_compensated(A,B,t,128);
  C4 = matmul(A,B,t,'new');

  err1(j) = max(max(abs(C1-C0)./C0));
  err2(j) = max(max(abs(C2-C0)./C0));
  err3(j) = max(max(abs(C3-C0)./C0));
  err4(j) = max(max(abs(C4-C0)./C0));
end

fs=14; ms=7; lw=1;
loglog(nlist, err1, '-o','markersize',ms,'linewidth',lw);
hold on;
loglog(nlist, err2, '-v','markersize',ms,'linewidth',lw);
loglog(nlist, err3, '-s','markersize',ms,'linewidth',lw);
loglog(nlist, err4, '-*','markersize',ms,'linewidth',lw);
hleg=legend('Compensated', 'FABsum ($$b=16$$)', 'FABsum ($$b=128$$)', 'New', 'Location', 'NorthEast');
set(hleg,'fontsize',fs,'interpreter','latex');
xlabel('$$n$$','interpreter','latex','fontsize',fs);
ylim([1e-4 1e-2]);
set(gca,'fontsize',fs);

function C = matmul_compensated(A,B,t)
  [m,n] = size(A);
  [~,p] = size(B);
  C = zeros(m,p);
  E = zeros(m,p);
  for k = 1:n
    T = C;
    Y = chop(E + chop(A(:,k)*B(k,:),t), t);
    C = chop(T+Y,t);
    E = chop(chop(T-C,t) + Y,t);
  end
end

function C = matmul_FABsum_compensated(A,B,t,nb)
  [m,n] = size(A);
  [~,p] = size(B);
  C = zeros(m,p);
  E = zeros(m,p);
  npart = ceil(n/nb);
  for k = 1:npart
    ibeg = 1+(k-1)*nb;
    iend = min(k*nb,n);
    T = C;
    Y = chop(E + matmul(A(:,ibeg:iend),B(ibeg:iend,:),t,'std'), t);
    C = chop(T+Y,t);
    E = chop(chop(T-C,t) + Y,t);
  end
end
